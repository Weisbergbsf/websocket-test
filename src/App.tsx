import { Socket } from './components/Socket'
import { Header } from "./components/Header";
import './styles/global.scss'


export function App() {
  return (
    <>
      <Header />
      <Socket /> 
    </>
  )
}