import { useState } from 'react'
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import SendIcon from '@mui/icons-material/Send';

import { Client, Versions } from '@stomp/stompjs';

import '../styles/socket.scss'

interface Config {
  url: string;
  topic: string;
  body: string;
}
let socket: Client;
export function Socket() {
  const [config, setConfig] = useState<Config>({ url: '', topic: '', body: '' });
  const [error, setError] = useState('');
  const [, setResult] = useState();

  function handleOnChangeConfig(event: any) {
    const { name, value } = event.target;
    setConfig(prevState => ({ ...prevState, [name]: value }))
  }

  function handleConfigWebSocket(event: any) {
    setError('');
    if (config.url && config.topic) {
      socket = new Client({
        stompVersions: new Versions(['1.2']),
        brokerURL: config.url,
      });
      socket.onConnect = function () {
        socket.subscribe(config.topic, (message) => {
          console.log(JSON.parse(message.body));
          setResult(JSON.parse(message.body))
        });
      };

      socket.onStompError = function (error) {
        setError(error?.headers?.message);
        console.warn(error);
        socket?.unsubscribe(config.topic);
        socket?.deactivate();
      };

      socket.onWebSocketError = function (error) {
        console.warn(error);
        socket?.unsubscribe(config.topic);
        socket?.deactivate();
      };

      socket.onUnhandledMessage = function (error) {
        console.warn(error);
      };

      socket.activate();
      console.log("Conectado")
    } else {
      console.log("Configure a URL ex.: ws://address.com.br/ws/websocket?access_token=eyJhbGc \n e o topic/subscribe")
      socket?.unsubscribe(config.topic);
      socket?.deactivate();
      setError('');
    }
  }

  return (
    <section className="task-list container">
      <header>
        <h2>Test WebSocket</h2>
      </header>

      <main>
        <div className="container-form">
          <div className="input-url-topic">
            <TextField
              required
              id="url"
              label="Url"
              type="text"
              name="url"
              placeholder="Add url"
              value={config.url}
              onChange={handleOnChangeConfig}
              sx={{ m: 1, width: '60%' }}
            />

            <TextField
              id="topic"
              label="Topic"
              type="text"
              name="topic"
              onChange={handleOnChangeConfig}
              value={config.topic}
              placeholder="Add topic"
              sx={{ m: 1, width: '40%' }}
            />
            <Button className="btn-send" variant="contained" endIcon={<SendIcon />} onClick={handleConfigWebSocket}>
              Connect
            </Button>
          </div>

          <p>{error}</p>
        </div>
      </main>
    </section>
  )
}